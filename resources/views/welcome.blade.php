<!-- Main templete -->
@extends('layouts.app')
<!--Title for page -->
@section('title', 'Questionnaire Welcome Page')
<!--Content in the page -->
@section('content')
    <!--Mian content inside the page -->
<div style="text-align: center">
    <h1>Welcome to my Questionnaire Home Page</h1><br>
    <p>Enter login details to continue or register</p><br>
    <p>If already logged in click button below to precede</p>

    <!--If user is already logged in this button allwos the user to continue -->
    <a href="{{ url('/home') }}" class="btn btn-xs btn-info pull-right">Click Here</a>

</div>


@endsection
