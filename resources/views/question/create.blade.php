<!-- Main templete -->
@extends('layouts.app')
<!--Title for page -->
@section('title', 'Create Question Page')
<!--Content in the page -->
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">



                    <div class="card-body">
                        <!--From in which teh questions and answers will be created -->
                        {!! Form::open(['url' => 'questionnaires/'. $questionnaire->id. '/questions']) !!}

                        @csrf
                        <!--This is the first question and answer form -->
                        <div class="form-group">

                            {!! Form::label('question', 'Question') !!}
                            {!! Form::text('questions', null, ['class' => 'form-control']) !!}

                            @error('questions')
                            <small class ="alert alert-danger">{{ $message }}</small>
                            @enderror
                        </div>
                        <div class="form-group">

                            {!! Form::label('answer1', 'Option 1') !!}
                            {!! Form::text('answer1', null, ['class' => 'form-control']) !!}

                            @error('answer1')
                            <small class ="alert alert-danger">{{ $message }}</small>
                            @enderror
                        </div>
                        <div class="form-group">

                            {!! Form::label('answer2', 'Option 2') !!}
                            {!! Form::text('answer2', null, ['class' => 'form-control']) !!}

                            @error('answer2')
                            <small class ="alert alert-danger">{{ $message }}</small>
                            @enderror
                        </div>
                        <div class="form-group">

                            {!! Form::label('answer3', 'Option 3') !!}
                            {!! Form::text('answer3', null, ['class' => 'form-control']) !!}

                            @error('answer3')
                            <small class ="alert alert-danger">{{ $message }}</small>
                            @enderror
                        </div>






                        <!--THis will submit the first form -->
                        <div class="form-group">
                            {!! Form::submit('Add Questions', ['class' => 'btn btn-primary form-control']) !!}
                        </div>
                    </div>




                    <!--This is the second question and answer form -->
                    <div class="form-group">

                        {!! Form::label('question1', 'Question') !!}
                        {!! Form::text('questions1', null, ['class' => 'form-control']) !!}
                        <!--Error message to allow user to knwo whats wrong -->
                        @error('questions1')
                        <small class ="alert alert-danger">{{ $message }}</small>
                        @enderror
                    </div>
                    <div class="form-group">

                        {!! Form::label('answer11', 'Option 1') !!}
                        {!! Form::text('answer11', null, ['class' => 'form-control']) !!}
                        <!--Error message to allow user to knwo whats wrong -->
                        @error('answer11')
                        <small class ="alert alert-danger">{{ $message }}</small>
                        @enderror
                    </div>
                    <div class="form-group">

                        {!! Form::label('answer12', 'Option 2') !!}
                        {!! Form::text('answer12', null, ['class' => 'form-control']) !!}
                        <!--Error message to allow user to knwo whats wrong -->
                        @error('answer12')
                        <small class ="alert alert-danger">{{ $message }}</small>
                        @enderror
                    </div>
                    <div class="form-group">

                        {!! Form::label('answer13', 'Option 3') !!}
                        {!! Form::text('answer13', null, ['class' => 'form-control']) !!}
                        <!--Error message to allow user to knwo whats wrong -->
                        @error('answer13')
                        <small class ="alert alert-danger">{{ $message }}</small>
                        @enderror
                    </div>






                    <!--THis will submit the second form -->
                    <div class="form-group">
                        {!! Form::submit('Add Questions', ['class' => 'btn btn-primary form-control']) !!}
                    </div>
                </div>




                <!--This is the third question and answer form -->
                <div class="form-group">

                    {!! Form::label('question2', 'Question') !!}
                    {!! Form::text('questions2', null, ['class' => 'form-control']) !!}
                    <!--Error message to allow user to knwo whats wrong -->
                    @error('questions2')
                    <small class ="alert alert-danger">{{ $message }}</small>
                    @enderror
                </div>
                <div class="form-group">

                    {!! Form::label('answer21', 'Option 1') !!}
                    {!! Form::text('answer21', null, ['class' => 'form-control']) !!}
                    <!--Error message to allow user to knwo whats wrong -->
                    @error('answer21')
                    <small class ="alert alert-danger">{{ $message }}</small>
                    @enderror
                </div>
                <div class="form-group">

                    {!! Form::label('answer22', 'Option 2') !!}
                    {!! Form::text('answer22', null, ['class' => 'form-control']) !!}
                    <!--Error message to allow user to knwo whats wrong -->
                    @error('answer22')
                    <small class ="alert alert-danger">{{ $message }}</small>
                    @enderror
                </div>
                <div class="form-group">

                    {!! Form::label('answer23', 'Option 3') !!}
                    {!! Form::text('answer23', null, ['class' => 'form-control']) !!}
                    <!--Error message to allow user to knwo whats wrong -->
                    @error('answer23')
                    <small class ="alert alert-danger">{{ $message }}</small>
                    @enderror
                </div>






                <!--THis will submit the third form -->
                <div class="form-group">
                    {!! Form::submit('Add Questions', ['class' => 'btn btn-primary form-control']) !!}
                </div>
            </div>








            {!! Form::close() !!}






                </div>
                </div>
            </div>
        </div>
    </div>
@endsection
