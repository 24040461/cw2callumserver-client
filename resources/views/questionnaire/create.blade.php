@extends('layouts.app')

@section('title', 'Create Questionnaire Page')
<!--Content in the page -->
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">

                    <div class="card-body">
                        <!-- Form for creating questionnaire -->
                        <div style="text-align: center">
                        <h1>Add New Questionnaire</h1>
                            <!--Form in which the questionnaire title and detail will be written in -->
                           {!! Form::open(['url' => 'questionnaires']) !!}

                            <div class="form-group">
                                {!! Form::label('title', 'Title:') !!}
                                {!! Form::text('title', null, ['class' => 'form-control']) !!}
                                <!--Error message to allow user to knwo whats wrong -->
                                @error('title')
                                <small class ="alert alert-danger">{{ $message }}</small>
                                @enderror
                            </div>

                            <div class="form-group">
                                {!! Form::label('details', 'Enter details:') !!}
                                {!! Form::textarea('detail', null, ['class' => 'form-control']) !!}
                                <!--Error message to allow user to knwo whats wrong -->
                                @error('detail')
                                <small class ="alert alert-danger">{{ $message }}</small>
                                @enderror
                            </div>


                            <!--THis will submit the questionnaire creation form -->
                            <div class="form-group">
                                {!! Form::submit('Add New questionnaire', ['class' => 'btn btn-primary form-control']) !!}
                            </div>
                        </div>


                            {!! Form::close() !!}







                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
