@extends('layouts.app')

@section('title', 'Created Questionnaire Page')
<!--Content in the page -->
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <!--User input for title showing when the user creates questionnaire -->

                    <div class="card-header">{{ $questionnaire->title }}</div>
                    <!--User input for detail showing when the user fills in detail -->

                    <div class="card-header">{{ $questionnaire->detail }}</div>

                    <div class="card-body">
                        <!--Create questions button to allow the user to create questions -->
                        <a class="btn btn-primary form-control" href="/questionnaires/{{ $questionnaire->id }}/questions/create">Add new question</a>


                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
