<!-- Main templete -->
@extends('layouts.app')
<!--Title for page -->
@section('title', 'Home Page')
<!--Content in the page -->
@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <!--Above the button title -->
                <div class="card-header">Questionnaire</div>


                    <!--button to allow user to carry if logged in -->
                    <a href="/questionnaires/create" class="btn btn-primary form-control">Create New Questionnaire</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
