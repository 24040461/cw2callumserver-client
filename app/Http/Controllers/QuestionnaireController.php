<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class QuestionnaireController extends Controller
{

    public function __construct()
    {
        //makes sure only logged in users can acceess
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //return to the view of questionnaire.create
        return view('questionnaire.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //validation for the questionnaire so user has to complete
        $info = request()->validate([
            'title' => 'required',
            'detail' => 'required',
        ]);



        //Reltionship between user and questionnaire

        $questionnaire = auth()->user()->questionnaires()->create($info);
        //redirect the user to the wanteed page
        return redirect('/questionnaires/'.$questionnaire->id);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Questionnaire $questionnaire
     * @return void
     */
    public function show(\App\Questionnaire $questionnaire)
    {
            $questionnaire->load('questions.answers');

            return view('questionnaire.show', compact('questionnaire'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
