<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Questionnaire;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class QuestionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Questionnaire $questionnaire
     * @return \Illuminate\Http\Response
     */
    public function create(Questionnaire $questionnaire)
    {
        //returning the view question.create
        return view('question.create', compact('questionnaire'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @param Questionnaire $questionnaire
     * @return void
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request, Questionnaire $questionnaire)
    {

        //valdiating the forms
        $this->validate($request,[
          'question' => 'required',
          'answer1' => 'required',
            'answer2' => 'required',
            'answer3' => 'required',

            'question1' => 'required',
            'answer11' => 'required',
            'answer12' => 'required',
            'answer13' => 'required',


            'question2' => 'required',
            'answer21' => 'required',
            'answer22' => 'required',
            'answer23' => 'required',




        ]);
        //gettign the user input
        $input = $request->all();

        question::create($input);
        //redirecting the user
        return redirect('/questionnaires/'.$questionnaire->id);



    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
