<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
        //information inside are for mass assigment
    protected $fillable = [
        'questionnaire_id',
        'question',
        'answer1',
        'answer2',
        'answer3',
    ];
        //relationships for quesrtionnaire and question
    public function questionnaire()
    {
        return $this->belongsTo('App\Questionnaire');
    }


}
