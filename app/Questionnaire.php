<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Questionnaire extends Model
{
    //title and detail are mass assigned
    protected $fillable = [
        'title',
        'detail',
    ];
   //relationships

    //relationships bewteen user and questionnaire
    public function user()
    {
        return $this->belongsTo('App\User');
    }
    //relationships between question and questionnaire
    public function questions()
    {
        return $this->hasMany('App\Question');
    }
}
